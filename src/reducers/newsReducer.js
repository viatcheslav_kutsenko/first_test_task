const initialState = {
    news: [], isLoading: false, loadError: false
}


export function loadNews(url) {
    return (dispatch) => {
        dispatch({
            type: 'IS_LOADING',
            payload: {isLoading: true}
        })

        fetch(url)
            .then((response) => {


                dispatch({
                    type: 'IS_LOADING',
                    payload: {isLoading: true}
                });

                return response;
            })
            .then((response) => response.json())
            .then((items) => {
                    dispatch({
                        type: 'LOAD_NEWS',
                        payload: {news: items.response.docs}
                    })
                }
            )
            .catch(() => dispatch({
                type: 'IS_LOAD_ERROR',
                payload: {}
            }));

    }

}


export function newsReducer(state = initialState, action) {

    switch (action.type) {
        case 'LOAD_NEWS':
            return {
                ...state,
                news: [action.payload.news[0], action.payload.news[1], action.payload.news[2], action.payload.news[3], action.payload.news[4], action.payload.news[5], action.payload.news[6], action.payload.news[7], action.payload.news[8], action.payload.news[9]],
                isLoading: false
            }
        case 'IS_LOADING':
            return {...state, news: [], isLoading: action.payload.isLoading, loadError: false}
        case 'IS_LOAD_ERROR':
            return {...state, isLoading: false, loadError: true}
        default:
            return state
    }
}

