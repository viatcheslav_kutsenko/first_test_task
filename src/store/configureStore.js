import {createStore, applyMiddleware, compose} from 'redux'
import rootReducer from '../reducers/index'
import thunk from 'redux-thunk';
import {routerMiddleware} from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

export const history = createHistory()


export default function configureStore(initialState) {
    const reactRouterMiddleware = routerMiddleware(history)
    const middlewares = [
        thunk,
        reactRouterMiddleware,
    ]

    return createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(...middlewares))
    )
}