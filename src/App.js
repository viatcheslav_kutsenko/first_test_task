import React, {Component} from 'react';
import NewsList from './NewsList';
import ViewNew from './ViewNew';
import {Switch, Route} from 'react-router-dom'
import logo from './Times-T-logo.jpg';

export default class App extends Component {


    render() {

        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">New York Times News</h1>
                </header>
                <main>
                    <Switch>
                        <Route exact path='/' component={NewsList}/>
                        <Route path='/new-details' component={ViewNew}/>
                    </Switch>
                </main>
                <footer className="App-footer">
                    <h3>{new Date().getFullYear()}</h3> {/*Current Year*/}
                </footer>
            </div>

        );
    }
}

