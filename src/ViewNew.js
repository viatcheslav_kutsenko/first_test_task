import React, {Component} from 'react';
import {connect} from 'react-redux'
import './App.css';
/*import {loadNews,setNewDetails} from './reducers/newsReducer';*/
import {bindActionCreators} from 'redux'
import {routerActions} from 'react-router-redux'
import {Link} from 'react-router-dom'


let buttonStyles = {
    position: 'relative',
    backgroundColor: '#e7e7e7',
    borderRadius: '15px',
    cursor: 'pointer',
    width: '100px'
}
class ViewNew extends Component {


    returnToNews = () => {
        let returnLink = localStorage.getItem(`returnLink`);
        this.props.history.push(returnLink)
    };

    render() {
        let fullText = localStorage.getItem(`fullText`);
        let imgUrl = localStorage.getItem(`imgUrl`);
        let returnLink = localStorage.getItem(`returnLink`);

        return (
            <div className="App">

                <div className="container">
                    <div className="content">
                        <div className="Posts" style={{minWidth:'600px'}}>
                            <article>

                                {imgUrl ? <img alt="" src={imgUrl}/> : null}

                                <p className="postBody">{fullText}</p>
                                <button>X</button>
                                <div>
                                    <button style={buttonStyles} onClick={this.returnToNews} type="button"><Link
                                        to={{ pathname: returnLink }}>Back</Link></button>
                                </div>
                            </article>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default connect(
    state => ({
        news: state
    }),
    dispatch => ({
        routerActions: bindActionCreators(routerActions, dispatch),
        dispatch
    })
)(ViewNew)
