import React, {Component} from 'react';
import {connect} from 'react-redux'
import './App.css';
import {loadNews} from './reducers/newsReducer';
import {bindActionCreators} from 'redux'
import {routerActions} from 'react-router-redux'


class NewsList extends Component {


    constructor(props) {
        super(props);
        const queryString = require('query-string');
        let year = 0;
        let month = 0;
        var url_params = queryString.parse(this.props.location.search);
        if (url_params['year'] && url_params['year'] > 0) {
            year = url_params['year'];
        }
        if (url_params['month'] && url_params['month'] > 0) {
            month = url_params['month'];
        }
        this.state = {year: year, month: month};
    }

    componentDidMount() {
        this.reloadNews();
    }


    changeBrowserURL = () => {
        let url = '';

        if (this.state.year > 0) {
            url = '?year=' + this.state.year;
        }

        if (this.state.month > 0) {
            if (url === '') {
                url = '?month=' + this.state.month;
            } else {
                url = url + '&month=' + this.state.month;
            }


        }


        this.props.history.push(url);
        this.reloadNews();
    }

    viewNew = (text, imgUrl) => {

        localStorage.setItem(`fullText`, text);
        localStorage.setItem(`imgUrl`, imgUrl);
        localStorage.setItem(`returnLink`, '/?year=' + this.state.year + '&month=' + this.state.month);
        this.props.history.push(`/new-details`)
    }

    reloadNews = () => {
        if (this.state.year > 0 && this.state.month > 0) {
            this.props.loadNews('http://api.nytimes.com/svc/archive/v1/' + this.state.year + '/' + this.state.month + '.json?api-key=d95522d9e2274709bd71cc88868bebb8');
        }

    }

    changeYearFilter = () => {
        this.setState({year: this.refs.yearFilter.value}, this.changeBrowserURL);
    }

    changeMonthFilter = () => {
        this.setState({month: this.refs.monthFilter.value}, this.changeBrowserURL);
    }


    render() {
        const currentYear = new Date().getFullYear();
        let yearsToSelect = [];
        for (let i = 0; i < 10; i++) {
            yearsToSelect.push(currentYear - i);
        }


        return (
            <div className="App">

                <div className="container">
                    <div className="content">
                        <nav>
                            <label>Year:
                                <select id="yearFilter" value={this.state.year} onChange={this.changeYearFilter}
                                        ref="yearFilter">
                                    <option value="" id={0}>No selected</option>
                                    {yearsToSelect.map((year) => (
                                        <option key={'city'+year} value={year}>{year}</option>
                                    ))}

                                </select>
                            </label>
                            <label>Month:


                                <select id="monthFilter" value={this.state.month} onChange={this.changeMonthFilter}
                                        disabled={this.state.year===0?true:false} ref="monthFilter">
                                    <option value="" id={0}>All</option>
                                    <option value="1" id="January">January</option>
                                    <option value="2" id="February">February</option>
                                    <option value="3" id="March">March</option>
                                    <option value="4" id="April">April</option>
                                    <option value="5" id="May">May</option>
                                    <option value="6" id="June">June</option>
                                    <option value="7" id="July">July</option>
                                    <option value="8" id="August">August</option>
                                    <option value="9" id="September">September</option>
                                    <option value="10" id="October">October</option>
                                    <option value="11" id="November">November</option>
                                    <option value="12" id="December">December</option>
                                </select>
                            </label>
                        </nav>
                        {this.props.news.newsReducer.isLoading === true ?
                            <div><img alt="Loading" src={require('./loader.gif')}/></div> : null}
                        {this.props.news.newsReducer.news && this.props.news.newsReducer.news.length > 0 ?
                            <div className="Posts">
                                {this.props.news.newsReducer.news.map((post, key) => (
                                    <div
                                        title="Click to view news details"
                                        style={{cursor:'pointer'}}
                                        onClick={() => this.viewNew(post.lead_paragraph,post.multimedia.length>0?'https://www.nytimes.com/'+post.multimedia[0].url:"")}
                                        className="Post" key={post._id} id={key}>
                                        <article>

                                            {post.multimedia.length > 0 ?
                                                <img alt="Preview"
                                                     src={'https://www.nytimes.com/'+post.multimedia[0].url}/> : null}

                                            <p className="postBody">{post.snippet}</p>
                                            <button>X</button>
                                        </article>
                                    </div>
                                ))}
                            </div> :
                            null}
                        {this.state.year === 0 || this.state.month === 0 ?
                            <h2>Please select Year and Month to load news</h2> : null
                        }


                    </div>
                </div>
            </div>
        );
    }
}


export default connect(
    state => ({
        news: state
    }),
    dispatch => ({
        loadNews: bindActionCreators(loadNews, dispatch),
        routerActions: bindActionCreators(routerActions, dispatch),
        dispatch
    })
)(NewsList)


